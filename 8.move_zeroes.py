def moveZeroes(nums):
    i = 0
    last=len(nums)-1
    while i<=last:
        if nums[i] == 0:
            nums.pop(i)
            nums.append(0)
            last -= 1
        else:
            i+=1
    return nums


list = [0,1,0,3,12]
list = [0,0,1]
print(moveZeroes(list))
