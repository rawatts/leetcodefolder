"""
Given two arrays, write a function to compute their intersection.
"""
from time import sleep
def intersect(nums1,nums2):
    intersection = []
    l1 = len(nums1)
    l2 = len(nums2)
    nums1.sort()
    nums2.sort()
    i,j =0,0
    while (i < l1 and j < l2):
        sleep(1)
        if nums1[i] > nums2[j]:
            sleep(1)
            j+=1
        else:
            if nums2[j]>nums1[i]:
                sleep(1)
                i+=1
            else:
                intersection.append(nums1[i])
                i+=1
                j+=1
    return intersection
    
    
nums1 = [1,2,2,1]
nums2 = [2,2]    
#nums1 = [4,9,5]
#nums2 = [9,4,9,8,4]
intersect(nums1,nums2)
