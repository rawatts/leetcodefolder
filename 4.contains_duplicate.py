"""Given an array of integers, find if the array contains any duplicates.

Your function should return true if any value appears at least twice in the array, and it should return false if every element is distinct.
"""
from collections import Counter
def containsDuplicate(nums):
    values = list(Counter(nums).values())
    return not all(i<2 for i in values)
    #return counts
    #n = len(nums)
    #for i in range (n):
        #if i < (n-1):
            #if nums.count(nums[i]) > 1:
                #return True
            #else:
                #pass
        #else:
            #return False
        
    #print(len(nums))
    #if len(nums) <= 1:
        #return False
    #else:
        #for i in range(len(nums)):
            #temp = list(nums)
            #temp.pop(i)
            #print(temp)

            #if nums[i] in temp:
                #return True
    #return False
            
            
#nums = [1,1,1,3,3,4,3,2,4,2]
#nums=[1,2,3,4]
#nums = [0]
nums = [2,14,18,22,22]
print(containsDuplicate(nums))
        
        
        
        
##
