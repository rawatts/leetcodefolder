def maxProfit(prices):
    n=len(prices)
    i=0
    profit = 0
    if n <= 1:
        return profit
    while i <(n-1):
        if prices[i] < prices[i+1]:
            profit += prices[i+1]-prices[i]
            i+=1
        else:
            i+=1
    return profit


#prices=[1,2,3,4,5]
prices = [7,1,5,3,6,4]
#prices = [7,1,5,3,6,4,1,2,3,4,5,1,2,3,4,5,7,1,5,3,6,4,1,2,3,4]
print(maxProfit(prices))
